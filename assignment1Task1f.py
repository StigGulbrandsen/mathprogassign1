import itertools as it

count = 0

#the dice loop

sampled = list(it.product(range(6), repeat = 6)) #sample of rolls 
eventd = list(it.permutations(range(6),5)) #relevant rolls

'''print (sampled)
print(len(eventd), "/", len(sampled))'''

'''
#the card loop

samplec = list(it.combinations(range(52), 5)) #sample of all card combinations
eventc = [i % 13 for i in samplec]

#n simulations
for i in len(samplec):
    for j in eventc:
        for k in eventc:
            if eventc[j] == eventc[k] and i != j:
                count += 1
'''            
#print(samplec)
print(count, "/", len(samplec))


#the queue loop

sampleq = list(it.permutations(range(5),5)) #sample of all permutations the queue can have
eventq = list(it.combinations(range(5),5)) #there is only one event where they're alphabetical, so this becomes 1

'''print(sampleq)
print(len(eventq), "/", len(sampleq))'''
