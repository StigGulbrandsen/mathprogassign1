
#include <stdio.h>
#include <random>

std::random_device rd;
std::default_random_engine generator(rd());
std::uniform_int_distribution<int> intDistro(1, 6);
std::uniform_real_distribution<float> floatDistro(1, 6);

int rando;
float randumb;
int main()
{
	rando = intDistro(generator);
	randumb = floatDistro(generator);
	printf("Uniform int distribution: %d\nUniform float distribution: %f\n", rando,randumb);

    return 0;
}

