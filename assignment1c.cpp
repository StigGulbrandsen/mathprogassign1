//task c in mathprog assignment 1 year 2019

#include <stdio.h>
#include <random>
#include <algorithm>
#include <array>

//counting every time the probability hits its mark
int count;
//N simulations 
const int N = 1000000;

//the wonderboys themselves, rando is for the dice range and rando2 is for the deck and queue
int rando[6];
int rando2[5];

std::random_device rd;
std::default_random_engine generator(rd());
std::uniform_int_distribution<int> dice(1, 6);
std::uniform_int_distribution<int> cards(1, 52);
std::uniform_int_distribution<int> queue(1, 5);


bool diceRolls(int k[6]) {
	for (int i = 0; i <= 5; i++)
		for (int j = i + 1; j <= 5; j++)
			if (k[i] == k[j])
				return false;
	return true;
}

bool queueHand(int k[5]) {
	//checks for duplicates
	for (int i = 0; i <= 4; i++)
		for (int j = i + 1; j <= 4; j++)
			if (k[i] == k[j])
				return false;
	return true;
}

bool trueQueue(int k[5]) {
	//checks for alphabetical order
	for (int i = 1; i <= 4; i++)
		if (k[i] < k[i - 1])
			return false;
	return true;
}

int main(void){
	//dice loop
	for (int i = 1; i <= N; i++) {

		for (int j = 0; j <= 5; j++)
			rando[j] = dice(generator);

		if (diceRolls(rando))
			count++;
	}

	printf("Chance of getting 6 different numbers on 6 dice in %d throws: %f\n", N, float(count) / N);

	//resets for next simulation
	count = 0;

	//card loop
	for (int i = 1; i <= N; i++) {
		//checks for duplicate numbers
		do
			for (int j = 0; j <= 4; j++)
				rando2[j] = cards(generator);
        while (!queueHand(rando2));
		//gets true value
			for (int j = 0; j <= 4; j++)
				rando2[j] = rando2[j] % 13;
		//checks for legal hands
		if (queueHand(rando2))
			count++;
	}
	printf("Chance of getting 5 different cards in a 5-card hand in %d simulations: %f\n", N, float(count) / N);
	//reset counter
	count = 0;

	//Queue loop
	for (int i = 1; i <= N; i++) {
		//checks for duplicate numbers
		do
			for (int j = 0; j <= 4; j++)
				rando2[j] = cards(generator);
        while (!queueHand(rando2));
		//checks if they're in alphabetical order
		if (trueQueue(rando2))
			count++;
	}
	printf("Chance of getting an alphabetical queue of 5 people in %d simulations: %f\n", N, float(count) / N);
}

