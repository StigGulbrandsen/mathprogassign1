import random as rd

nrolls = 25000000000
hits = 0.00
count = False
done = False
hand = list(range(5))  
suits = list(range(5)) 

for i in range(nrolls):
    done = False
    while done == False:
        done = True
        for j in range(5): 
            hand[j] = rd.randint(1,13)
            suits[j] = rd.randint(1,4)
            
            for j in range(5):
                for k in range(5): 
                    if j != k and hand[j] == hand[k] and suits[j] == suits[k]: 
                        done = False 

    count = True
    for j in range(5):
        for k in range(5):
            if j != k and hand[j] == hand[k]:
                count = False
    if count == True:
        hits += 1
        
print (hits/nrolls)






