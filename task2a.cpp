#include <eigen3/Eigen/Dense>
#include <iostream>
#include <cstdio>

const int max = 38;

//Lovely matrix
Eigen::MatrixXd snakesAndLadders(max,max);

float nevner = 6;

void emptySnakes();
void createShortcut(int fromState, int toState);
void rollCheck();
void reset(int c, Eigen::MatrixXd l);

//Where the magic happens
int main(){
    emptySnakes();
    createShortcut(2,1);
    createShortcut(7,15);
    createShortcut(16,12);
    std::cout << snakesAndLadders << std::endl;
    rollCheck();
    return 0;
}

void emptySnakes(){

    snakesAndLadders.setZero();
        //double for loop for floating point 1/6 assignments
        for (int j = 0; j <= 37; j++){ 
            //value changes when it gets closer to the end
            /*if (j > 30)
                nevner --;*/
            for (int k = j+1; k < j+7; k++){
                if (k < 38)
                    snakesAndLadders(k, j) = 1/nevner;
                else{
                    snakesAndLadders(37, j) += 1/nevner;
                    //snakesAndLadders(37, 37) = 0;
                }
            }
        }
}

void createShortcut(int fromState, int toState){
    for (int i = 0; i<= 37; i++){
        //moves all the states from fromstate over to tostate
        if (snakesAndLadders(fromState,i) != 0)
            snakesAndLadders(toState,i) += 1/nevner; 
        //sets the fromstate to 0
        snakesAndLadders(fromState,i) = 0;    
    } 
    //places the new toState in the same x coordinate as fromstate 
}

//the 3 last tasks
void rollCheck(){
    int count = 1;
    //least rolls check
     Eigen::MatrixXd least = snakesAndLadders;
     do{
         least *= snakesAndLadders;
         count++;
     }while(least (37, 0) == 0);
     
    std::cout << "\nChance of completing the game:"<< least(37,0) << std::endl << "Least number of rolls: " << count << std::endl;

    //50 percent check
    least = snakesAndLadders; count = 1;
    do{
         least *= snakesAndLadders;
         count++;
     }while(least (37, 0) <= 0.5);
    std::cout << "\nChance of completing the game:"<< least(37,0) << std::endl << "Number of rolls for more than 50 percent probability of finishing the game: " << count << std::endl;

    //99 percent check
    least = snakesAndLadders; count = 1;
    do{
         least *= snakesAndLadders;
         count++;
     }while(least (37, 0) <= 0.99);
     std::cout << "\nChance of completing the game:"<< least(37,0) << std::endl << "Number of rolls for more than 99 percent probability of finishing the game: " << count << std::endl;

     //average check
     least = snakesAndLadders; count = 1;
     do{
        least*= snakesAndLadders;
        count++;
     }while(least(37,0) < 1);
    std::cout << "\nChance of completing the game:"<< least(37,0) << std::endl << "Number of rolls for 100 percent probability of finishing the game: " << count << std::endl;

    std::cout << "\nAverage chance to complete the game in 28 rolls: " << least(37,0)/count << std:: endl;

    //roll check
    float avg = least(37,0)/count;
    least = snakesAndLadders; count = 1;
     do{
        least*= snakesAndLadders;
        count++;
     }while(least(37,0) < avg);
    std::cout << "Average number of rolls needed to complete the game: " << count << std:: endl;
    
}